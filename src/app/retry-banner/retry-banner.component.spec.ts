import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetryBannerComponent } from './retry-banner.component';

describe('RetryBannerComponent', () => {
  let component: RetryBannerComponent;
  let fixture: ComponentFixture<RetryBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetryBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetryBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
