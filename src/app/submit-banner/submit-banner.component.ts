import { Component, OnInit } from '@angular/core';
// import { CookieService } from 'angular2-cookie/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-submit-banner',
  templateUrl: './submit-banner.component.html',
  styleUrls: ['./submit-banner.component.sass']
})
export class SubmitBannerComponent implements OnInit {
  public isAuthorized;

  constructor(private _cookieService: CookieService) { }

  ngOnInit() {
    const token = this._cookieService.get('authorized_access_token');
    this.isAuthorized = !(token === null || token === undefined || token === '');
  }
}
