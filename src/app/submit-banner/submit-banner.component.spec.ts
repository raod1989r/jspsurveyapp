import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitBannerComponent } from './submit-banner.component';

describe('SubmitBannerComponent', () => {
  let component: SubmitBannerComponent;
  let fixture: ComponentFixture<SubmitBannerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmitBannerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
