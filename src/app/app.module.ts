import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCardModule} from '@angular/material/card';
import {MatMenuModule} from '@angular/material/menu';
import {MatListModule} from '@angular/material/list';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonModule} from '@angular/material/button';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SurveyformComponent } from './surveyform/surveyform.component';
import { SubmitBannerComponent } from './submit-banner/submit-banner.component';
import { RetryBannerComponent } from './retry-banner/retry-banner.component';
import { AuthorizedUsersComponent } from './authorized-users/authorized-users.component';
import { LoginComponent } from './authorized-users/login/login.component';
import { ForgotPasswordComponent } from './authorized-users/forgot-password/forgot-password.component';
import { DashboardComponent } from './authorized-users/dashboard/dashboard.component';
import { RegisterPhoneComponent } from './authorized-users/register-phone/register-phone.component';
import { SubmitMessageComponent } from './authorized-users/submit-message/submit-message.component';
// import { CookieService } from 'angular2-cookie/services/cookies.service';
import { CookieService } from 'ngx-cookie-service';
import { EditUserComponent } from './shared/edit-user/edit-user.component';
import {AuthService} from './services/auth.service';

@NgModule({
  declarations: [
    AppComponent,
    SurveyformComponent,
    SubmitBannerComponent,
    RetryBannerComponent,
    AuthorizedUsersComponent,
    LoginComponent,
    ForgotPasswordComponent,
    DashboardComponent,
    RegisterPhoneComponent,
    SubmitMessageComponent,
    EditUserComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    MatCardModule,
    MatMenuModule,
    MatDividerModule,
    MatListModule,
    MatButtonModule,
    MatSnackBarModule,
    MatProgressBarModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatIconModule,
    MatToolbarModule
  ],
  providers: [
    CookieService,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
