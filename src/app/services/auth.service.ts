import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AuthService {
    private subject = new Subject<any>();

    sendLogin() {
        this.subject.next();
    }

    getLogin(): Observable<any> {
        return this.subject.asObservable();
    }
}