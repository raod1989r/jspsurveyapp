import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import { AccountKit, AuthResponse } from 'ng2-account-kit';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

declare var AccountKitPlugin: any;

@Component({
  selector: 'app-surveyform',
  templateUrl: './surveyform.component.html',
  styleUrls: ['./surveyform.component.sass']
})
export class SurveyformComponent implements OnInit {
  public loginSuccess = false;
  public matSnackbarConfig;
  public internetFetch = false;
  public data = {};

  constructor(private http: HttpClient, public snackBar: MatSnackBar,  public router: Router, public route: ActivatedRoute) {
    this.matSnackbarConfig = new MatSnackBarConfig();
    this.matSnackbarConfig.duration = 3000;
  }

  ngOnInit() {
    if (!navigator.onLine) {
      this.internetNotActive();
    } else {
      // setTimeout(this.login(), 50000);
    }
  }

  internetNotActive() {
    this.snackBar.open('Please connect to internet', '', this.matSnackbarConfig);
  }

  errorLogin(error) {
    this.loginSuccess = false;
  }

  successfullLogin(response) {
    this.setupFormData(response);
  }

  login(): any {
    if (!navigator.onLine) {
      this.internetNotActive();
    } else {
      try {
        AccountKit.login('PHONE', { countryCode: '+91', phoneNumber: '' }).then(
          (response: AuthResponse) => this.successfullLogin(response),
          (error: any) => this.errorLogin(error)
        );
      } catch (err) {
        console.log(err);
      }

      try {
        AccountKitPlugin.loginWithPhoneNumber(
            {defaultCountryCode: 'IN',
              facebookNotificationsEnabled: false},
            (response) => this.successfullLogin(response),
            (error: any) => this.errorLogin(error)
      );
      } catch (err) {
        console.log(err);
      }
    }
  }

  setupFormData(response) {
   this.internetFetch = true;

   this.http.post(`${environment.BASE_URL}survey-form-data`, response).subscribe((data) => {
     this.data = data;
     this.loginSuccess = true;
     this.internetFetch = false;
   },
   error => {
     this.snackBar.open('Please try again', '', this.matSnackbarConfig);
     this.internetFetch = false;
   }
   );
  }
}
