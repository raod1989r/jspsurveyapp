import { Component, OnInit, Input, OnChanges } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
// import { CookieService } from 'angular2-cookie/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.sass']
})
export class EditUserComponent implements OnInit, OnChanges {
  @Input() data;
  @Input() individualForm;

  public parties = [];
  public designations = [];
  public boothNos = [];
  public socialGroups = [];
  public willingToWorkOptions = [];
  public educationCategories = [];
  public mobilePhoneCommunications = [];
  public noOfVoters = [];
  public ageList = [];
  public selectOtherFamilyPersons = [];
  public genders = [];
  public jSPPartyId;
  public selectFamilyId;
  public matSnackbarConfig;
  public internetFetch = false;

  public selectedParams = {
    name: null,
    address: null,
    phone_number: null,
    party_id: null,
    age_list_id: null,
    gender_id: null,
    no_of_voters_in_family: null,
    designation_id: null,
    booth_no_id: null,
    social_group_id: null,
    willing_to_work_id: null,
    education_id: null,
    mobile_phone_communicate_id: null,
    select_other_family_person_id: null,
    other_family_members_willing_to_works: []
  };

  public submitButtonState = false;

  constructor(private http: HttpClient, public snackBar: MatSnackBar,  public router: Router, public route: ActivatedRoute, private _cookieService: CookieService) {
    this.matSnackbarConfig = new MatSnackBarConfig();
    this.matSnackbarConfig.duration = 3000;
  }

  ngOnChanges() {
    this.setupFormData(this.data);
  }

  ngOnInit() {
  }

  setJSPPartyID(partyCode) {
    const jSPParty = this.parties.filter((party) => {
      return (party.name === partyCode);
    });

    this.jSPPartyId = jSPParty[0]['id'];
  }

  setSelectFamilyYesId() {
    const familyYesList = this.selectOtherFamilyPersons.filter((selection) => {
      return (selection.desc === 'Yes');
    });

    this.selectFamilyId = familyYesList[0]['id'];
  }

  checkJSPParty() {
    return (this.jSPPartyId === this.selectedParams.party_id);
  }

  setupFormData(data) {
    this.selectedParams = data['user'];
    this.parties = data['parties'];
    this.designations = data['designations'];
    this.boothNos = data['booth_nos'];
    this.socialGroups = data['social_groups'];
    this.willingToWorkOptions = data['willing_to_works'];
    this.educationCategories = data['educations'];
    this.genders = data['genders'];
    this.selectOtherFamilyPersons = data['select_other_family_persons'];
    this.ageList = data['age_lists'];
    this.mobilePhoneCommunications = data['mobile_phone_communicates'];
    this.noOfVoters = Array.from(Array(data['max_voters']).keys());
    this.selectedParams['access_token'] = data['access_token'];

    const partyCode = data['jsp_code'];
    // this.addFamilyPerson();
    this.setJSPPartyID(partyCode);
    this.setSelectFamilyYesId();
    this.activateSubmitButton(null, '');
  }

  addFamilyPerson() {
    const personsLength: number = this.selectedParams.other_family_members_willing_to_works.length;

    this.selectedParams.other_family_members_willing_to_works.push({id: (personsLength), name: '', phone: ''});
  }

  deleteFamilyPerson(id) {
    this.selectedParams.other_family_members_willing_to_works =
        this.selectedParams.other_family_members_willing_to_works.filter((person) => {
          return (person.id !== id);
        });
  }

  activateSubmitButton(event, field) {
    if (field === 'social_group_id') {
      this.selectedParams.social_group_id = event.source.value;
    } else if (field === 'select_other_family_person_id') {
      this.selectedParams.select_other_family_person_id = event.source.value;
    }

    if ((this.checkJSPParty() && this.selectedParams.select_other_family_person_id !== null) ||
        (this.selectedParams.social_group_id !== null && !this.checkJSPParty())) {

      if (this.selectedParams.select_other_family_person_id === this.selectFamilyId) {
        if (this.selectedParams.other_family_members_willing_to_works.length === 0) {
          this.addFamilyPerson();
        }
      } else if (this.selectedParams.select_other_family_person_id === 'No') {
        this.selectedParams.other_family_members_willing_to_works = [];
      }
      this.submitButtonState = true;
    } else {
      this.submitButtonState = false;
    }
  }

  clearJSPDetails() {
    // this.selectedParams.party_id = event.source.value;

    if (!this.checkJSPParty()) {
      this.selectedParams.no_of_voters_in_family = null;
      this.selectedParams.willing_to_work_id = null;
      this.selectedParams.education_id = null;
      this.selectedParams.age_list_id = null;
      this.selectedParams.select_other_family_person_id = null;
      this.selectedParams.other_family_members_willing_to_works = [];
      this.activateSubmitButton(null, null);
    } else {
      this.selectedParams.party_id = this.jSPPartyId;
    }
  }

  submit() {
    if (this.checkJSPParty() &&
        (this.selectedParams.no_of_voters_in_family === null ||
        this.selectedParams.willing_to_work_id === null ||
        this.selectedParams.education_id === null ||
        this.selectedParams.age_list_id === null ||
        this.selectedParams.select_other_family_person_id === null)
    ) {

      this.submitButtonState = false;
      this.snackBar.open('Please fill details', '', this.matSnackbarConfig);

      return;
    }

    if (!this.checkJSPParty()) {
      this.clearJSPDetails();
    }

    if (!this.individualForm) {
      this.selectedParams['authorized_user_access_token'] = this._cookieService.get('authorized_access_token');
    }

    this.internetFetch = true;

    this.http.post(`${environment.BASE_URL}survey`, {user: this.selectedParams}).subscribe((resp) => {
          if (resp['status'] === 'success') {
            this.showSubmittedBanner();
          } else {
            this.showRetryBanner();
          }
          this.internetFetch = false;
        },
        error => {
          this.showRetryBanner();
          this.internetFetch = false;
        }
    );
  }

  showSubmittedBanner() {
    this.router.navigate(['submitForm']);
  }

  showRetryBanner() {
    this.snackBar.open('Please retry submit', '', this.matSnackbarConfig);
    // this.router.navigate(['retryForm']);
  }
}
