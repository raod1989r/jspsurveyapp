import { NgModule } from '@angular/core';
import {SurveyformComponent} from './surveyform/surveyform.component';
import {SubmitBannerComponent} from './submit-banner/submit-banner.component';
import {RetryBannerComponent} from './retry-banner/retry-banner.component';
import {LoginComponent} from './authorized-users/login/login.component';
import {DashboardComponent} from './authorized-users/dashboard/dashboard.component';
import {ForgotPasswordComponent} from './authorized-users/forgot-password/forgot-password.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  // { path: '', redirectTo: '/survey', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'retryForm', component: RetryBannerComponent },
  { path: 'submitForm', component: SubmitBannerComponent },
  { path: 'survey', component: SurveyformComponent },
  { path: '**', component: SurveyformComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
