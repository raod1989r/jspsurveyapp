import { Component, OnInit } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
// import { CookieService } from 'angular2-cookie/core';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  hide = true;
  public email = '';
  public password = '';
  public matSnackbarConfig;

  constructor(public snackBar: MatSnackBar, private http: HttpClient, public router: Router, private authService: AuthService,
              private _cookieService: CookieService) {
    this.matSnackbarConfig = new MatSnackBarConfig();
    this.matSnackbarConfig.duration = 3000;
  }

  ngOnInit() {
  }

  login() {
    if (this.email.trim() !== '' || this.password.trim() !== '') {
      const data = {
        email: this.email,
        password: this.password
      };

      this.http.post(`${environment.BASE_URL}login`, data).subscribe((response) => {
        if (response['status'] === 'failure') {
          this.snackBar.open(response['message'], '', this.matSnackbarConfig);
        } else {
          this._cookieService.set('authorized_access_token', response['data']['login']['access_token']);
          this.authService.sendLogin();
          this.router.navigate(['dashboard']);
        }
      }, error => {
        this.snackBar.open('Please try again', '', this.matSnackbarConfig);
      });

      } else {
      this.snackBar.open('Enter details to continue', '', this.matSnackbarConfig);
    }
  }
}
