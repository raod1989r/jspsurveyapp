import { Component, OnInit } from '@angular/core';
// import { CookieService } from 'angular2-cookie/core';
import { CookieService } from 'ngx-cookie-service';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit {
  public users = [];
  public newUserPhone = '';
  public loginSuccess = false;
  public newUserData = {};
  public matSnackbarConfig;

  constructor(private _cookieService: CookieService, private http: HttpClient, public router: Router, public snackBar: MatSnackBar) {
    this.matSnackbarConfig = new MatSnackBarConfig();
    this.matSnackbarConfig.duration = 3000;
  }

  ngOnInit() {
    const token = {token: this._cookieService.get('authorized_access_token')};

    this.http.post(`${environment.BASE_URL}get_users`, token).subscribe((response) => {
      if (response['status'] === 'success') {
        this.users = response['users']['users'];
      } else {
        this.invalidToken();
      }
    }, error => {
      this.invalidToken();
    });
  }

  invalidToken() {
    this._cookieService.deleteAll();
    this.router.navigate(['/']);
  }

  editUser(phone) {
    const data = {
      authorized_user_access_token: this._cookieService.get('authorized_access_token'),
      phone_number: phone
    };

    this.http.post(`${environment.BASE_URL}create_new_user_authority`, data).subscribe((response) => {
      if (response['user'] === null) {
        this.snackBar.open('Invalid authorized request', '', this.matSnackbarConfig);
      } else {
        this.loginSuccess = true;
        this.newUserData = response;
      }
    }, error => {
      this.invalidToken();
    });
  }

  cancel() {
    this.loginSuccess = false;
  }

  addNewUser() {
    if (this.newUserPhone.toString().length === 10) {
      this.editUser(this.newUserPhone);
    } else {
      this.snackBar.open('Invalid phone', '', this.matSnackbarConfig);
    }
  }
}
