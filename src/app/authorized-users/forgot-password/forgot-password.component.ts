import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.sass']
})
export class ForgotPasswordComponent implements OnInit {
  public email = '';
  public isForgotSuccess = false;
  public matSnackbarConfig;

  constructor(private http: HttpClient, public snackBar: MatSnackBar) {
    this.matSnackbarConfig = new MatSnackBarConfig();
    this.matSnackbarConfig.duration = 3000;
  }

  ngOnInit() {
  }

  submit() {
    if (this.email.trim().length > 0) {
      const data = {email: this.email};

      this.http.post(`${environment.BASE_URL}forgot/password`, data).subscribe((response) => {
        this.isForgotSuccess = true;
      });
    } else {
      this.snackBar.open('Please enter valid details', '', this.matSnackbarConfig);
    }
  }
}
