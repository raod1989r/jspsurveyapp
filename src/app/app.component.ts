import {MediaMatcher} from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
// import { CookieService } from 'angular2-cookie/core';
import { CookieService } from 'ngx-cookie-service';
import {environment} from '../environments/environment';
import {HttpClient} from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import {AuthService} from './services/auth.service';

declare var device;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})

export class AppComponent implements OnInit, OnDestroy {
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;
  shouldRun = true;
  fillerNav = [];
  public isLogout = true;
  subscription: Subscription;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher, public router: Router,
              private authService: AuthService, private _cookieService: CookieService, private http: HttpClient) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
    this.subscription = this.authService.getLogin().subscribe(message => { this.resetFiltersToLogin(); });
  }

  ngOnInit() {
    document.addEventListener('deviceready', function() {
      // alert(device.platform);
    }, false);
    this.setFilters();
  }

  setFilters() {
    const token = this._cookieService.get('authorized_access_token');

    if (token === null || token === undefined || token === '') {
      this.resetFiltersToLogout();
    } else {
      this.resetFiltersToLogin();
    }
  }

  resetFiltersToLogin() {
    this.isLogout = false;
    this.fillerNav = [];
  }

  resetFiltersToLogout() {
    this.isLogout = true;

    this.fillerNav = [
      { path: 'survey', name: 'Registration Form' },
      { path: 'login', name: 'Member Login' }
    ];
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  logout() {
    const data = {token: this._cookieService.get('authorized_access_token')};

    this.http.post(`${environment.BASE_URL}logout`, data).subscribe((response) => {
      this._cookieService.deleteAll();
      this.resetFiltersToLogout();
      this.router.navigate(['/']);
    });
  }
}
