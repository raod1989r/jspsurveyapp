const express = require('express');
const app = express();
const cors = require('cors');
const compression = require('compression');

app.use(cors());
app.options('*', cors());
app.use(compression());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(express.static('heroku'));

const port = process.env.PORT || 3000;

app.listen(port);
