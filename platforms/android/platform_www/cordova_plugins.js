cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
  {
    "id": "cordova-plugin-device.device",
    "file": "plugins/cordova-plugin-device/www/device.js",
    "pluginId": "cordova-plugin-device",
    "clobbers": [
      "device"
    ]
  },
  {
    "id": "cordova-plugin-accountkit.plugin",
    "file": "plugins/cordova-plugin-accountkit/www/plugin.js",
    "pluginId": "cordova-plugin-accountkit",
    "clobbers": [
      "AccountKitPlugin"
    ],
    "runs": true
  }
];
module.exports.metadata = 
// TOP OF METADATA
{
  "cordova-plugin-whitelist": "1.3.3",
  "cordova-plugin-device": "2.0.2",
  "cordova-plugin-accountkit": "1.4.0"
};
// BOTTOM OF METADATA
});